package com.behavioral.templatepattern;

public class Main {
    public static void main(String[] args) {
        HouseTemplate house1 = new WoodenHouse();
        house1.buildHouse();
    }
}
