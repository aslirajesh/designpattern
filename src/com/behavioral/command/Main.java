package com.behavioral.command;

import com.behavioral.command.fx.AddCustomerCommand;
import com.behavioral.command.fx.Button;
import com.behavioral.command.fx.Command;

public class Main {
    public static void main(String[] args) {
        CustomerService service = new CustomerService();
        Command command = new AddCustomerCommand(service);
        Button button = new Button(command);
        button.click();
    }
}
