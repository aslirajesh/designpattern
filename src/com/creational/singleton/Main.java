package com.creational.singleton;

public class Main {
    public static void main(String[] args) {
        ConfigManager manager = ConfigManager.getInstance();
        manager.setSettings("name", "rajesh");
        System.out.println(manager.getSettings());

        ConfigManager other = ConfigManager.getInstance();
        other.setSettings("mode", "dark");

        System.out.println(other.getSettings());
    }
}
