package com.behavioral.strategy;

public class ImageStorage {
    private final Compressor compressor;

    public ImageStorage(Compressor compressor) {
        this.compressor = compressor;
    }

    public String compress(String fileName){
        return compressor.compress(fileName);
    }
}
