package com.behavioral.templatepattern;

public class WoodenHouse extends HouseTemplate{
    @Override
    protected void buildPillars() {
        System.out.println("build pillars with wood");
    }

    @Override
    protected void buildWalls() {
        System.out.println("build walls with wood");
    }
}
