package com.behavioral.templatepattern;

public abstract class HouseTemplate {
    public final void buildHouse(){
        buildFoundation();
        buildPillars();
        buildWalls();
        buildWindows();
        System.out.println("your house is ready to move");
    }

    private void buildFoundation(){
        System.out.println("foundation built with concrete");
    }

    private void buildWindows(){
        System.out.println("windows built with wood");
    }

    protected abstract void buildPillars();
    protected abstract void buildWalls();
}
