package com.behavioral.iterator;

import java.util.Stack;

public class BrowseHistory {
    //private final List<String> urls = new ArrayList<>();
    private final Stack<String> urls = new Stack<>();

    public void push(String s){
        urls.push(s);
    }

    public Iterator<String> createIterator(){
        return new StackIterator(this);
    }

//    public static class ListIterator implements Iterator<String>{
//        private final BrowseHistory history;
//        private int index;
//        public ListIterator(BrowseHistory history){
//            this.history = history;
//        }
//
//        @Override
//        public boolean hasNext() {
//            return index < history.urls.size();
//        }
//
//        @Override
//        public String currrent() {
//            return history.urls.get(index++);
//        }
//
//        @Override
//        public void next() {
//            index++;
//        }
//    }

    public static class StackIterator implements Iterator<String>{
        private final BrowseHistory history;
        private int index;

        public StackIterator(BrowseHistory history) {
            this.history = history;
        }

        @Override
        public boolean hasNext() {
            return !history.urls.empty();
        }

        @Override
        public String currrent() {
            return history.urls.pop();
        }

        @Override
        public void next() {
            history.urls.pop();
        }
    }
}
