package com.taxCalculator;

public class main {
    public static void main(String[] args) {
        TaxCalculator calculator = getCalculator();
        calculator.calculateTax();
    }

    public static TaxCalculator getCalculator(){
        return new TaxCalculator2019();
    }
}
