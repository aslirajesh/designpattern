package com.behavioral.observer;

public interface Observer {
    void update(int n);
}
