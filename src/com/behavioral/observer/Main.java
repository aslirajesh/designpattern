package com.behavioral.observer;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DataSource(12);
        Observer sheet1 = new SpreadSheet();
        Observer sheet2 = new SpreadSheet();
        Observer chart = new Chart();

        dataSource.addObserver(sheet1);
        dataSource.addObserver(sheet2);
        dataSource.addObserver(chart);

        dataSource.setValue(13);
    }
}
