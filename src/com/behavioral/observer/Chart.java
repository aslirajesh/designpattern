package com.behavioral.observer;

public class Chart implements Observer{
    @Override
    public void update(int n) {
        System.out.println("chart updated" + n);
    }
}
