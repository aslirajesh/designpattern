package com.behavioral.state;

public class Selection implements Tool {
    @Override
    public void mouseUp() {
        System.out.println("draw a dashed rectangle");
    }

    @Override
    public void mouseDown() {
        System.out.println("selection icon");
    }
}
