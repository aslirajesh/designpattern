package com.behavioral.state;

public class Eraser implements Tool{

    @Override
    public void mouseDown() {
        System.out.println("Earser Icon");
    }

    @Override
    public void mouseUp() {
        System.out.println("Erasing Something");
    }
}
