package com.behavioral.templatepattern;

public class ConcreteHouse extends HouseTemplate{
    @Override
    protected void buildPillars() {
        System.out.println("build pillars with concrete");
    }

    @Override
    protected void buildWalls() {
        System.out.println("build walls with concrete");
    }
}
