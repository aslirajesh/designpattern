package com.behavioral.iterator;

public interface Iterator<T> {
    boolean hasNext();
    T currrent();
    void next();
}
