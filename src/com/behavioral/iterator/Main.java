package com.behavioral.iterator;

public class Main {
    public static void main(String[] args) {
        BrowseHistory browseHistory = new BrowseHistory();
        browseHistory.push("a");
        browseHistory.push("b");
        browseHistory.push("c");

        Iterator<String> historyIterator = browseHistory.createIterator();
        while(historyIterator.hasNext()){
            System.out.println(historyIterator.currrent());

        }
    }
}
