package com.taxCalculator;

public interface TaxCalculator {
    float calculateTax();

}
