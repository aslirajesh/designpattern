package com.behavioral.strategy;

public class Main {
    public static void main(String[] args) {
        ImageStorage ims = new ImageStorage(new PngCompressor());
        System.out.println(ims.compress("untitled.txt"));
    }
}
