package com.behavioral.strategy;

public class JpgCompressor implements Compressor{
    @Override
    public String compress(String fileName) {
        return fileName + " compressed using jpeg compressor";
    }
}
