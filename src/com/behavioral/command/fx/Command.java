package com.behavioral.command.fx;

public interface Command {
    void execute();
}
