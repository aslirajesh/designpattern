package com.behavioral.strategy;

public interface Compressor {
    String compress(String fileName);
}
