package com.creational.singleton;

import java.util.HashMap;
import java.util.Map;

public class ConfigManager {
    private final Map<String, String> settings = new HashMap<>();
    private ConfigManager(){}

    private static ConfigManager instance ;
    public static ConfigManager getInstance(){
        if (instance == null){
            instance = new ConfigManager();
        }
        return instance;
    }

    public Map<String, String> getSettings() {
        return settings;
    }

    public void setSettings(String key, String value) {
        this.settings.put(key, value);
    }
}
