package com.behavioral.strategy;

public class PngCompressor implements Compressor{
    @Override
    public String compress(String fileName) {
        return fileName + " compressed using png compressor";
    }
}
