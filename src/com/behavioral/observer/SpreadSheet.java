package com.behavioral.observer;

public class SpreadSheet implements Observer{

    @Override
    public void update(int n) {
        System.out.println("spreadsheet updated" + n);
    }
}
